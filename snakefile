
rule maak_holocene_deklaag:
	input:
		r"data\1-external\REGIS_v2_2.nc",
		r"data\1-external\GeoTOP.nc",
		r"data\1-external\GeoTOP_k_values.csv"
	output:
		r"data\2-interim\holocene_deklaag_c.nc",
	script:
		"src/1-prepare/holocene deklaag.py"

rule maak_schematisatie:
	input:
		r"data\2-interim\holocene_deklaag_c.nc"
	output:
		r"data\2-interim\ondergrond.nc"
	script:
		"src/1-prepare/maak_schematisatie_LHM.py"

rule maak_chloride:
	input:
		r"data\1-external\chloride.nc",
		r"data\2-interim\ondergrond.nc"
	output:
		r"data\2-interim\chloride_data.nc"
	script:
		"src/1-prepare/chloride.py"

rule aantal_wvp_per_laag

rule schematisatie per wvp

rule maak_opbarstingsindex:
	input:
		r"data\1-external\opbarstings_index_peilvast_T0_ditch1_0m.asc",
		r"data\2-interim\chloride_data.nc"
	output:
		r"data\2-interim\opbarstings_index.nc"
	script:
		"src/1-prepare/opbarstingsindex.py"

rule maak_stroming:
	input:
		r"data\1-external\Achtergrondstroming\w001001.adf",
		r"data\2-interim\chloride_data.nc"
	output:
		r"data\2-interim\achtergrondstroming.nc"
	script:
		"src/1-prepare/achtergrondstroming.py"

rule maak_qmin:
	input:
		r"data\2-interim\chloride_data.nc",
		r"data\2-interim\ondergrond.nc"
	output:
		r"data\2-interim\qmin.nc"
	script:
		"src/1-prepare/qmin.py"

rule maak_kdwaarden:
	input:
		r"data\2-interim\ondergrond.nc"
	output:
		r"data\2-interim\kd_waarden.nc"
	script:
		"src/1-prepare/kdwaarden.py"	

rule maak_ASR_categorie:
	input:
		r"data\2-interim\achtergrondstroming.nc",
		r"data\2-interim\qmin.nc",
		r"data\2-interim\opbarstings_index.nc",
		r"data\2-interim\kd_waarden.nc",
		r"data\2-interim\ondergrond.nc"
	output:
		"data/3-input/ASR_cat.nc"
	script:
		"src/2-build/ASR_categorie.py"

rule maak_brakwaterwinning_categorie:
	input:
		r"data\2-interim\chloride_data.nc",
		r"data\2-interim\kd_waarden.nc",
		r"data\2-interim\ondergrond.nc"
	output:
		"data/3-input/brak_cat.nc"
	script:
		"src/2-build/brakwaterwinning_categorie.py"

rule maak_brakwaterwinning_kansrijkheid:
	input:
		"data/3-input/brak_cat.nc"
	output:
		r"data\4-output\kansrijkheid_brakwaterwinning.nc"
	script:
		"src/3-model/Brakwaterwinning_kansrijkheid.py"

