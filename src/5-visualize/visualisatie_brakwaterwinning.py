# -*- coding: utf-8 -*-
"""
Dit script visualiseert de resultaten voor de Geschiktheid voor brakwaterwinning
en de bijbehorende categorien.
"""
import xarray as xr
import matplotlib
import matplotlib.pyplot as plt
import geopandas as gpd
import imod
import seaborn as sns
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd

def select_area(da, path):
    shape = gpd.read_file(path)
    da = da.sel(
        y=slice(shape.bounds.maxy[0], shape.bounds.miny[0]),
        x=(slice(shape.bounds.minx[0], shape.bounds.maxx[0])),
    )
    return da

def plot_map(
    raster,
    colors,
    labels,
    overlays=[],
    title=None,
    kwargs_raster=None,
    kwargs_legend=None,
    logos = None
):
    """
    Parameters
    ----------
    raster : xr.DataArray
        2D grid to plot with categorical data from 0 to n. 
    colors : list of str, or list of RGB tuples
        Matplotlib acceptable list of colors. Length N.
        Accepts both tuples of (R, G, B) and hexidecimal (e.g. "#7ec0ee").

        
    labels : listlike of floats or integers
        Boundaries between the legend colors/classes. Length: N - 1.
    overlays : list of dicts, optional
        Dicts contain geodataframe (key is "gdf"), and the keyword arguments
        for plotting the geodataframe.
    kwargs_raster : dict of keyword arguments, optional
        These arguments are forwarded to ax.imshow()
    kwargs_colorbar : dict of keyword arguments, optional
        These arguments are forwarded to fig.colorbar()
    """
    
    colors = colors.copy()
    labels = labels.copy()
    raster = raster.where(~raster.isnull(),len(colors))
    raster = raster.where(mask.notnull())
    colors.append('#D3D3D3')
    labels.append('LHM-laag niet aanwezig')
    ncolors = len(colors)
    nlabels = len(labels)
    levels = list(range(0,nlabels))[1:]
    if not nlabels == ncolors:
        raise ValueError(
            f"Incorrect number of labels. Number of colors is {ncolors},"
            f"got {nlabels} labels instead."
        )

    # Get extent
    _, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(raster)

    # Read legend settings
    cmap, norm = imod.visualize.common._cmapnorm_from_colorslevels(colors, levels)
    # raster kwargs
    settings_raster = {"interpolation": "nearest", "extent": [xmin, xmax, ymin, ymax]}
    if kwargs_raster is not None:
        settings_raster.update(kwargs_raster)
    # legend kwargs
    settings_legend = {'loc': 'upper left', 'frameon': False}
    if kwargs_raster is not None:
        settings_raster.update(kwargs_raster)

    # Make figure
    if logos == None:
        len_logos = 1
    else:
        len_logos = len(logos)
        
    gs = gridspec.GridSpec(2, len_logos, height_ratios=[24, 2])
    ax = plt.subplot(gs[0, :])
    ax.imshow(raster, cmap=cmap, norm=norm, **settings_raster)
    ax.figure.set_figwidth(10)
    ax.figure.set_figheight(10)

    # Set ax limits
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # Add title
    # if title is not None:
    #     ax.set_title(title, pad=140, loc='left', fontsize=24)
    values = raster.to_dataframe()[raster.name].value_counts()
    values = values.index.values.astype(int)
    values = sorted(values[~np.isnan(values)].astype(int))
  
    # create a legend-item for every color in the raster
    legend_item = [matplotlib.patches.Patch(color=colors[i], label=labels[i]) for i in values]
    legend_raster = plt.legend(handles=legend_item, bbox_to_anchor=(0, 1.28), **settings_legend)
    ax.add_artist(legend_raster)

    # Add overlays
    labels_overlays = []
    for overlay in overlays:
        tmp = overlay.copy()
        gdf = tmp.pop("gdf")

        gdf.plot(ax=ax, **tmp)
        labels_overlays.append(tmp)
    legend_overlay = [matplotlib.patches.Patch(**i) for i in labels_overlays]
    ax.legend(handles=legend_overlay, bbox_to_anchor=(0.40, 1.28), **settings_legend)

    # Add logo
    if logos != None:
        
        for i, logo in enumerate(logos):
            ax2 = plt.subplot(gs[1, i])
            ax2.imshow(logo)
            ax2.axis('off')

    plt.savefig(f"data/5-visualization/Brakwaterwinning/{title}", dpi=300, bbox_inches='tight')
    plt.close()


sns.set(style="whitegrid", palette="pastel", color_codes=True)

# Path data
path_brak = "data/4-output/kansrijkheid_brakwaterwinning.nc"
path_brak_cat = "data/3-input/brak_cat.nc"
path_cl =r"data\2-interim\chloride_data.nc"
path_dikte =  r"data\4-output\kansrijk_brak_dikte.tif"
path_diepte = r"data\4-output\kansrijk_brak_diepte.tif"

# Paths overlays
path_boezem = r"data\1-external\Kaarten\Boezems\Boezemwater.shp"
path_zee = r"data\1-external\Kaarten\Zee\Zee.shp"
path_provincie = r"data\1-external\Kaarten\Provincies\Provincies.shp"
path_bebouwing = r"data\1-external\Kaarten\Bebouwing\bebouwd.shp"
path_wadden =  r"data\1-external\Kaarten\wadden.shp"

# Paths gebieden
path_MT = r"data\1-external\Shapes\MTpolder.shp"
path_Noordplas = r"data\1-external\Shapes\NOORDPLAS_V2.shp"
path_Dunea = r"data\1-external\Shapes\meijendel-berkheide.shp"
path_Haarlemmermeer = r"data\1-external\Shapes\HAARLEMMERMEERPOLDER.shp"
path_Horstermeer = r"data\1-external\Shapes\horstermeer.shp"
path_Dinteloord = r"data\1-external\Shapes\Dinteloord.shp"
path_Westland = r"data\1-external\Shapes\Westland.shp"
path_ZH = r"data\1-external\Model_grens\Model_grens.shp"

shapes = [path_ZH, path_MT,path_Noordplas,path_Dunea,path_Haarlemmermeer,path_Horstermeer]
shapes_name = ['Zuid-Holland','MT-polder','Noordplas','Dunea','Haarlemmermeer','Horstermeer']

shapes_totaal = shapes[1:-1]
cases = gpd.GeoDataFrame()
for shape in shapes_totaal:
    df = gpd.read_file(shape)
    try:
        cases = cases.union(df)
    except:
        cases = df

# Overlays
shapes_cases = {"gdf":cases,
         'facecolor':'none',
         'edgecolor':'black',
         "label":"Cases"}
boezem = {"gdf": gpd.read_file(path_boezem).boundary,
          "linewidth": 0.1,
          "edgecolor": 'grey',
          'facecolor': 'none',
          'label':'Boezem'}
zee = {"gdf": gpd.read_file(path_zee), 
       "linewidth": 0.1,
       "facecolor": "#a6cee3",
       "alpha": 0.9,
       'label':'Zee'}
provincies = {'gdf': gpd.read_file(path_provincie),
              'facecolor': 'none',
              'linewidth':0.5,
              'alpha':0.5,
              'edgecolor': '#1f78b4',
              'label': 'Provincies'}
bebouwing = {"gdf": gpd.read_file(path_bebouwing), 
             "alpha":0.23,
             "color":"#a0a0a0",
             'label':'Bebouwing'}
wadden =  {"gdf": gpd.read_file(path_wadden), 
             "alpha":0.8,
             "linewidth":0.05,
             "edgecolor":"black",
             "facecolor":'none',
             "hatch":"XXX",
             'label':"Onbekend"}

overlays = [provincies, zee, boezem, wadden]
overlay_cases = [shapes_cases, provincies, zee, boezem,wadden]

# Logos
# deltares = image.imread(r'data\1-external\Kaarten\Deltares.png')
# kwr = image.imread(r'data\1-external\Kaarten\KWR.png')
# arcadis = image.imread(r'data\1-external\Kaarten\Arcadis.png')
logos = None #[deltares, kwr, arcadis]

# Laad data
brak = xr.open_dataset(path_brak)['brak_mean']
kansrijk_brak_dikte = xr.open_dataset(path_dikte)['top']
kansrijk_brak_diepte = xr.open_dataset(path_diepte)['__xarray_dataarray_variable__']

brak_cat = xr.open_dataset(path_brak_cat)
cl = xr.open_dataset(path_cl)

min_diepte_brak = cl['max_diepte_brak']

mask = gpd.read_file(path_provincie)
mask = mask[mask.PROVC_NM.isin(['Groningen','Friesland','Noord-Holland','Utrecht','Zuid-Holland','Zeeland','Flevoland'])]
mask = imod.prepare.rasterize(mask, like=brak_cat['brak_mean'].sel(layer=1))

# Brakwaterwinning categoriën
# Brak_mean veranderen, zodat te zoet en te zout ook worden weergegeven
brak_mean = brak_cat['brak_mean'].where(~(brak_cat['brak_mean'].isnull() & (cl['cl_mean'] <4000)), 3)
brak_mean = brak_mean.where(~(brak_cat['brak_mean'].isnull() & (cl['cl_mean'] >4000)), 0)
brak_mean = brak_mean.where(~brak_cat['kd'].isnull(), np.nan)

dikte_brak = brak_cat['dikte_brak']
dikte_brak = dikte_brak.where(~(brak_cat['dikte_brak'].isnull() & (cl['cl_mean'] <4000)), 3)
dikte_brak = dikte_brak.where(~(brak_cat['dikte_brak'].isnull() & (cl['cl_mean'] >4000)), 4)
dikte_brak = dikte_brak.where(~brak_cat['kd'].isnull(), np.nan)

kd = brak_cat['kd']

maxdiepte = brak_cat['max_diepte_brak']
maxdiepte = maxdiepte.where(~(brak_cat['max_diepte_brak'].isnull() & (cl['cl_mean'] <4000)), 3)
maxdiepte = maxdiepte.where(~(brak_cat['max_diepte_brak'].isnull() & (cl['cl_mean'] >=4000)), 4)
maxdiepte = maxdiepte.where(~brak_cat['kd'].isnull(), np.nan)

brak = brak.where(~brak_cat['kd'].isnull(), np.nan)
brak = brak.where(~(brak==2),1)
brak = brak.where(~(brak==3),1)
brak = brak.where(~(brak==4),2)
brak = brak.where(~(brak==5),3)
brak = brak.where(~(brak==6),4)
brak_totaal = brak.max(dim='layer')
brak = brak.where(~(brak_cat['brak_mean'].isnull() & (cl['cl_mean'] <4000)), 5)
brak = brak.where(~(brak_cat['brak_mean'].isnull() & (cl['cl_mean'] >=4000)), 6)
brak_zout = brak.max(dim='layer')
brak_totaal = brak_totaal.where(brak_totaal.notnull(),brak_zout)
brak_totaal = brak_totaal.where(mask.notnull())

# Brak totaal
labels7 = ['Niet geschikt', 'Mogelijk geschikt', 'Geschikt (klein)',
              'Geschikt (groot)','Geschikt (beide)', 'Te zoet','Te zout']
colors7 = ['#d73027','#ffffbf','#d9ef8b','#91cf60','#1a9850', '#92c5de','#f4a582']
plot_map(brak_totaal, colors7, labels7, overlays, 'Geschiktheid Brakwaterwinning zonder cases', logos=logos)
labels7 = ['Niet geschikt', 'Mogelijk geschikt', 'Geschikt (klein)',
              'Geschikt (groot)','Geschikt (beide)', 'Te zoet','Te zout']
colors7 = ['#d73027','#ffffbf','#d9ef8b','#91cf60','#1a9850', '#92c5de','#f4a582']
plot_map(brak_totaal, colors7, labels7, overlay_cases, 'Geschiktheid Brakwaterwinning', logos=logos)

for l in brak.layer:
    labels7 = ['Niet geschikt', 'Mogelijk geschikt', 'Geschikt (klein)',
              'Geschikt (groot)','Geschikt (beide)', 'Te zoet','Te zout']
    colors7 = ['#d73027','#ffffbf','#d9ef8b','#91cf60','#1a9850', '#92c5de','#f4a582']
    plot_map(brak.sel(layer=l), colors7, labels7, overlays, f'Geschiktheid Brakwaterwinning LHM-laag{int(l)}', logos=logos)

for l in brak_mean.layer:
    labelsbrak = ['Te zout', 'Mogelijk geschikt', 'Geschikt', 'Te zoet']
    colorsbrak = ['#f4a582', '#ffffbf', '#1a9850', '#92c5de']
    plot_map(brak_mean.sel(layer=l), colorsbrak, labelsbrak, overlays, f'Cl-concentratie brakwaterpakket LHM-laag{int(l)}', logos=logos)

for l in dikte_brak.layer:
    colors3 = ['#d73027', '#d9ef8b', '#1a9850', '#92c5de','#f4a582']
    labels3 = ['Niet geschikt (te dun)', 'Geschikt (kleinschalig)', 'Geschikt (beide)', 'Te zoet','Te zout']
    plot_map(dikte_brak.sel(layer=l), colors3, labels3, overlays, f'Dikte brakwaterpakket LHM-laag{int(l)}', logos=logos)

for l in kd.layer:
    colors3a = ['#d73027', '#d9ef8b', '#1a9850']
    labels3a = ['Niet geschikt', 'Geschikt (kleinschalig)', 'Geschikt (beide)']
    plot_map(kd.sel(layer=l), colors3a, labels3a, overlays, f'Doorlaatvermogen LHM-laag{int(l)}', logos=logos)

for l in maxdiepte.layer:
    colors3b = ['#d73027', '#1a9850', '#d9ef8b', '#92c5de','#f4a582']
    labels3b = ['Niet geschikt (te diep)', 'Geschikt (beide)', 'Geschikt (grootschalig)', 'Te zoet','Te zout']
    plot_map(maxdiepte.sel(layer=l), colors3b, labels3b, overlays, f'Diepte brakwaterpakket LHM-laag{int(l)}', logos=logos)


mask = gpd.read_file(path_provincie)
mask = imod.prepare.rasterize(mask, like=brak_cat['brak_mean'].sel(layer=1))

min_diepte = pd.DataFrame()
# Per gebiedje
for shape, name in zip(shapes, shapes_name):
    braktotaal_zoom = select_area(brak_totaal, shape)
    brak_zoom = select_area(brak,shape)
    brak_mean_zoom = select_area(brak_mean,shape)
    dikte_brak_zoom = select_area(dikte_brak,shape)
    kd_zoom = select_area(kd, shape)
    maxdiepte_zoom = select_area(maxdiepte, shape)
    
    min_diepte_zoom = select_area(kansrijk_min_diepte,shape)
    min_diepte.loc[name, 'median'] = min_diepte_zoom.where((braktotaal_zoom>0) & (braktotaal_zoom <5)).median()
    min_diepte_zoom.plot()
    plt.savefig(f'data/5-visualization/Brakwaterwinning/{name}\Min_diepte{name}')
    plt.close()
    labels7 = ['Niet geschikt', 'Mogelijk geschikt', 'Geschikt (klein)',
              'Geschikt (groot)','Geschikt (beide)', 'Te zoet','Te zout']

    colors7 = ['#d73027','#ffffbf','#d9ef8b','#91cf60','#1a9850', '#92c5de','#f4a582']
    
    plot_map(braktotaal_zoom, colors7, labels7, overlays, title= f'{name}\Geschiktheid Brakwaterwinning {name}', logos=logos)
    for l in brak.layer:
        plot_map(brak_zoom.sel(layer=l), colors7, labels7, overlays, f'{name}\Geschiktheid Brakwaterwinning {name} LHM-laag{int(l)}', logos=logos)
    
    
    labelsbrak = ['Te zout', 'Mogelijk geschikt', 'Geschikt', 'Te zoet']
    colorsbrak = ['#f4a582', '#ffffbf', '#1a9850', '#92c5de']
    
    for l in brak_mean.layer:
        plot_map(brak_mean_zoom.sel(layer=l), colorsbrak, labelsbrak, overlays, f'{name}\Cl-concentratie brakwaterpakket {name} LHM-laag{int(l)}', logos=logos)
    
    colors3 = ['#d73027', '#d9ef8b', '#1a9850', '#92c5de','#f4a582']
    labels3 = ['Niet geschikt (te dun)', 'Geschikt (kleinschalig)', 'Geschikt (beide)', 'Te zoet','Te zout']
    for l in dikte_brak.layer:
        plot_map(dikte_brak_zoom.sel(layer=l), colors3, labels3, overlays, f'{name}\Dikte brakwaterpakket {name} LHM-laag{int(l)}', logos=logos)
    
    colors3a = ['#d73027', '#d9ef8b', '#1a9850']
    labels3a = ['Niet geschikt', 'Geschikt (kleinschalig)', 'Geschikt (beide)']
    for l in kd.layer:
        plot_map(kd_zoom.sel(layer=l), colors3a, labels3a, overlays, f'{name}\Doorlaatvermogen {name} LHM-laag{int(l)}', logos=logos)
    
    colors3b = ['#d73027', '#1a9850', '#d9ef8b', '#92c5de','#f4a582']
    labels3b = ['Niet geschikt (te diep)', 'Geschikt (beide)', 'Geschikt (grootschalig)', 'Te zoet','Te zout']
    for l in maxdiepte.layer:
        plot_map(maxdiepte_zoom.sel(layer=l), colors3b, labels3b, overlays, f'{name}\Diepte brakwaterpakket {name} LHM-laag{int(l)}', logos=logos)
