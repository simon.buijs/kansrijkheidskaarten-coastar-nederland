_# -*- coding: utf-8 -*-
"""
Dit script visualiseert de resultaten voor de kansrijkheid voor ASR en de
bijbehorende categorien. 
"""
import xarray as xr
import matplotlib
import matplotlib.pyplot as plt
import geopandas as gpd
import imod
import matplotlib.image as image
import seaborn as sns
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd

def select_area(da, path):
    shape = gpd.read_file(path)
    da = da.sel(
        y=slice(shape.bounds.maxy[0], shape.bounds.miny[0]),
        x=(slice(shape.bounds.minx[0], shape.bounds.maxx[0])),
    )
    return da

def plot_map(
    raster,
    colors,
    labels,
    overlays=[],
    title=None,
    kwargs_raster=None,
    kwargs_legend=None,
    logos = None
):
    """
    Parameters
    ----------
    raster : xr.DataArray
        2D grid to plot with categorical data from 0 to n. 
    colors : list of str, or list of RGB tuples
        Matplotlib acceptable list of colors. Length N.
        Accepts both tuples of (R, G, B) and hexidecimal (e.g. "#7ec0ee").

        
    labels : listlike of floats or integers
        Boundaries between the legend colors/classes. Length: N - 1.
    overlays : list of dicts, optional
        Dicts contain geodataframe (key is "gdf"), and the keyword arguments
        for plotting the geodataframe.
    kwargs_raster : dict of keyword arguments, optional
        These arguments are forwarded to ax.imshow()
    kwargs_colorbar : dict of keyword arguments, optional
        These arguments are forwarded to fig.colorbar()
    """
    
    
    
    raster = raster.where(~raster.isnull(),len(colors))
    raster = raster.where(mask.notnull())
    colors.append('#D3D3D3')
    labels.append('LHM-laag niet aanwezig')
    ncolors = len(colors)
    nlabels = len(labels)
    levels = list(range(0,nlabels))[1:]
    if not nlabels == ncolors:
        raise ValueError(
            f"Incorrect number of labels. Number of colors is {ncolors},"
            f"got {nlabels} labels instead."
        )

    # Get extent
    _, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(raster)

    # Read legend settings
    cmap, norm = imod.visualize.common._cmapnorm_from_colorslevels(colors, levels)
    # raster kwargs
    settings_raster = {"interpolation": "nearest", "extent": [xmin, xmax, ymin, ymax]}
    if kwargs_raster is not None:
        settings_raster.update(kwargs_raster)
    # legend kwargs
    settings_legend = {'loc': 'upper left', 'frameon': False}
    if kwargs_raster is not None:
        settings_raster.update(kwargs_raster)

    # Make figure
    if logos == None:
        len_logos = 1
    else:
        len_logos = len(logos)
        
    gs = gridspec.GridSpec(2, len_logos, height_ratios=[24, 2])
    ax = plt.subplot(gs[0, :])
    ax.imshow(raster, cmap=cmap, norm=norm, **settings_raster)
    ax.figure.set_figwidth(10)
    ax.figure.set_figheight(10)

    # Set ax limits
    ax.set_xlim(xmin, xmax)
    ax.set_ylim(ymin, ymax)
    # Add title
    # if title is not None:
    #     ax.set_title(title, pad=140, loc='left', fontsize=24)
    values = raster.to_dataframe()[raster.name].value_counts()
    values = values.index.values.astype(int)
    values = sorted(values[~np.isnan(values)].astype(int))
  
    # create a legend-item for every color in the raster
    legend_item = [matplotlib.patches.Patch(color=colors[i], label=labels[i]) for i in values]
    legend_raster = plt.legend(handles=legend_item, bbox_to_anchor=(0, 1.28), **settings_legend)
    ax.add_artist(legend_raster)

    # Add overlays
    labels_overlays = []
    for overlay in overlays:
        tmp = overlay.copy()
        gdf = tmp.pop("gdf")

        gdf.plot(ax=ax, **tmp)
        labels_overlays.append(tmp)
    legend_overlay = [matplotlib.patches.Patch(**i) for i in labels_overlays]
    ax.legend(handles=legend_overlay, bbox_to_anchor=(0.40, 1.28), **settings_legend)

    # Add logo
    if logos != None:
        
        for i, logo in enumerate(logos):
            ax2 = plt.subplot(gs[1, i])
            ax2.imshow(logo)
            ax2.axis('off')

    plt.savefig(f"data/5-visualization/ASR/{title}", dpi=300, bbox_inches='tight')
    plt.close()


sns.set(style="whitegrid", palette="pastel", color_codes=True)

# Path data
path_asr = "data/4-output/kansrijkheid_ASR.nc"
path_asr_cat = "data/3-input/ASR_cat.nc"


# Paths overlays
path_boezem = r"data\1-external\Kaarten\Boezems\Boezemwater.shp"
path_zee = r"data\1-external\Kaarten\Zee\Zee.shp"
path_provincie = r"data\1-external\Kaarten\Provincies\Provincies.shp"
path_brabant = r'data\1-external\Kaarten\Provincies\Zoutbrabant.shp'
path_bebouwing = r"data\1-external\Kaarten\Bebouwing\bebouwd.shp"
path_wadden =  r"data\1-external\Kaarten\wadden.shp"

# Paths gebieden
path_Dinteloord = r"data\1-external\Shapes\Dinteloord.shp"
path_Westland = r"data\1-external\Shapes\Westland.shp"
path_hoorn = r'data\1-external\Shapes\asr_hoorn.shp'
path_ZH = r"data\1-external\Model_grens\Model_grens.shp"

shapes = [path_ZH, path_Dinteloord,path_Westland, path_hoorn]
shapes_name = ['Zuid-Holland','Dinteloord','Westland', 'Hoorn']


shapes_totaal = shapes[1:-1]
cases = gpd.GeoDataFrame()
for shape in shapes_totaal:
    df = gpd.read_file(shape)
    try:
        cases = cases.union(df)
    except:
        cases = df
# Overlays
wadden_shape = gpd.read_file(path_wadden)
# provincie = gpd.read_file(path_provincie)
# flevoland = provincie[provincie.PROVC_NM=='Flevoland']
# wadden_shape = wadden_shape.append(flevoland)

# Overlays
shapes_cases = {"gdf":cases,
         'facecolor':'none',
         'edgecolor':'black',
         "label":"Cases"}
boezem = {"gdf": gpd.read_file(path_boezem).boundary,
          "linewidth": 0.1,
          "edgecolor": 'grey',
          'facecolor': 'none',
          'label': 'Boezem'}
zee = {"gdf": gpd.read_file(path_zee),
       "linewidth": 0.1,
       "facecolor": "#a6cee3",
       "alpha": 0.9,
       'label': 'Zee'}
provincies = {'gdf': gpd.read_file(path_provincie),
              'facecolor': 'none',
              'linewidth':0.5,
              'alpha':0.5,
              'edgecolor': '#1f78b4',
              'label': 'Provincies'}
bebouwing = {"gdf": gpd.read_file(path_bebouwing),
             "alpha": 0.23,
             "color": "#a0a0a0",
             'label': 'Bebouwing'}
wadden =  {"gdf": wadden_shape,
             "alpha":0.8,
             "linewidth":0.05,
             "edgecolor":"black",
             "facecolor":'none',
             "hatch":"XXX",
             'label':"Onbekend"}
overlays = [provincies, zee, boezem, wadden]
overlay_cases = [shapes_cases, provincies, zee, boezem, wadden]

# Logos
deltares = image.imread(r'data\1-external\Kaarten\Deltares.png')
kwr = image.imread(r'data\1-external\Kaarten\KWR.png')
arcadis = image.imread(r'data\1-external\Kaarten\Arcadis.png')
logos = None #[deltares, kwr, arcadis]

# Laad data
asr = xr.open_dataset(path_asr)['qmin']
asr_totaal = asr.max(dim='layer')
asr_cat = xr.open_dataset(path_asr_cat)

# Mask
mask = gpd.read_file(path_provincie)
zoutbrabant = gpd.read_file(path_brabant).to_crs(epsg=28992)
zeeland = gpd.overlay(mask[mask['PROVC_NM']=='Zeeland'],zoutbrabant,how='union')
zeeland['PROVC_NM'] = 'Zeeland'
mask.loc[mask.PROVC_NM=='Zeeland','geometry'] = zeeland.dissolve(by='PROVC_NM')['geometry'].values
mask = mask[mask.PROVC_NM.isin(['Groningen','Friesland','Noord-Holland','Utrecht','Zuid-Holland','Zeeland'])]
mask = imod.prepare.rasterize(mask, like=asr_cat['qmin'].sel(layer=1))

labels7 = ['Niet geschikt', 'Mogelijk geschikt (klein)',
              'Mogelijk geschikt (groot)', 'Mogelijk geschikt (beide)', 'Geschikt (klein)',
              'Geschikt (groot)', 'Geschikt (beide)']
colors7 = ['#d73027', '#fc8d59', '#fee08b', '#ffffbf', '#d9ef8b', '#91cf60', '#1a9850']

# ASR totaal
plot_map(asr_totaal, colors7, labels7, overlays, 'Geschiktheid ASR zonder cases', logos=logos)
plot_map(asr_totaal, colors7, labels7, overlay_cases, 'Geschiktheid ASR', logos=logos)

# ASR per wvp
for l in asr.layer:
    labels7 = ['Niet geschikt', 'Mogelijk geschikt (klein)',
              'Mogelijk geschikt (groot)', 'Mogelijk geschikt (beide)', 'Geschikt (klein)',
              'Geschikt (groot)', 'Geschikt (beide)']
    colors7 = ['#d73027', '#fc8d59', '#fee08b', '#ffffbf', '#d9ef8b', '#91cf60', '#1a9850']
    plot_map(asr.sel(layer=l), colors7, labels7, overlays, f'Geschiktheid ASR LHM-laag {int(l)}', logos=logos)

# ASR_categorien
stroming = asr_cat['stroming']
qmin = asr_cat['qmin']
opbarsting = asr_cat['opbarsting']
kd = asr_cat['kd']

labels2 = ['Niet geschikt', 'Geschikt']
colors2 = ['#d73027', '#1a9850']
plot_map(opbarsting, colors2, labels2, overlays, 'Opbarstrisico', logos=logos)

for l in stroming.layer:
    labels2 = ['Niet geschikt', 'Geschikt']
    colors2 = ['#d73027', '#1a9850']
    plot_map(stroming.sel(layer=l), colors2, labels2, overlays, f'Achtergrondstroming LHM-laag {int(l)}', logos=logos)
    
for l in qmin.layer:
    labels3 = ['Niet geschikt', 'Geschikt (beide)', 'Geschikt (groot)']
    colors3 = ['#d73027', '#1a9850', '#d9ef8b']
    plot_map(qmin.sel(layer=l), colors3, labels3, overlays, f'Opdrijving LHM-laag {int(l)}', logos=logos)

for l in kd.layer:
    labels3a = ['Niet geschikt', 'Geschikt (kleinschalig)', 'Geschikt (beide)']
    colors3a = ['#d73027', '#d9ef8b', '#1a9850']
    plot_map(kd.sel(layer=l), colors3a, labels3a, overlays, f'Doorlaatvermogen LHM-laag {int(l)}', logos=logos)


mask = gpd.read_file(path_provincie)
mask = imod.prepare.rasterize(mask, like=asr_cat['qmin'].sel(layer=1))




min_diepte = pd.DataFrame()
# Per gebiedje
for shape, name in zip(shapes, shapes_name):
    
    # # ASR_categorien
    stroming = select_area(asr_cat['stroming'], shape)
    qmin = select_area(asr_cat['qmin'], shape)
    opbarsting = select_area(asr_cat['opbarsting'], shape)
    kd = select_area(asr_cat['kd'], shape)
    asrtotaal_zoom = select_area(asr_totaal, shape)
    asr_zoom = select_area(asr, shape)
    min_diepte_zoom = select_area(kansrijk_min_diepte,shape)
    min_diepte.loc[name, 'median'] = min_diepte_zoom.where(asrtotaal_zoom>0).median()
    min_diepte_zoom.plot()
    plt.savefig(f'data/5-visualization/ASR/{name}\Min_diepte{name}')
    plt.close()
    
    labels7 = ['Niet geschikt', 'Mogelijk geschikt (klein)',
              'Mogelijk geschikt (groot)', 'Mogelijk geschikt (beide)', 'Geschikt (klein)',
              'Geschikt (groot)', 'Geschikt (beide)']

    colors7 = ['#d73027', '#fc8d59', '#fee08b', '#ffffbf', '#d9ef8b', '#91cf60', '#1a9850']

    # ASR totaal
    plot_map(select_area(asrtotaal_zoom, shape), colors7, labels7, overlays, f'{name}\Geschiktheid ASR {name}', logos=logos)
    
    # ASR per wvp
    for l in asr_zoom.layer:
        try:
            labels7 = ['Niet geschikt', 'Mogelijk geschikt (klein)',
              'Mogelijk geschikt (groot)', 'Mogelijk geschikt (beide)', 'Geschikt (klein)',
              'Geschikt (groot)', 'Geschikt (beide)']
            colors7 = ['#d73027', '#fc8d59', '#fee08b', '#ffffbf', '#d9ef8b', '#91cf60', '#1a9850']
            plot_map(asr_zoom.sel(layer=l), colors7, labels7, overlays, f'{name}\Geschiktheid ASR {name} LHM-laag {int(l)}', logos=logos)
        except:
            continue
    
    for l in stroming.layer:
        labels2 = ['Niet geschikt', 'Geschikt']
        colors2 = ['#d73027', '#1a9850']
        plot_map(stroming.sel(layer=l), colors2, labels2, overlays, f'{name}\Achtergrondstroming {name} LHM-laag {int(l)}', logos=logos)
        
    labels2 = ['Niet geschikt', 'Geschikt']
    colors2 = ['#d73027', '#1a9850']
    plot_map(opbarsting, colors2, labels2, overlays, f'{name}\Opbarstrisico {name} LHM-laag ', logos=logos)
 
    for l in qmin.layer:
        labels3 = ['Niet geschikt', 'Geschikt (beide)', 'Geschikt (groot)']
        colors3 = ['#d73027', '#1a9850', '#d9ef8b']
        plot_map(qmin.sel(layer=l), colors3, labels3, overlays, f'{name}\Opdrijving {name} LHM-laag {int(l)}', logos=logos)
    
    for l in kd.layer:
        labels3a = ['Niet geschikt', 'Geschikt (kleinschalig)', 'Geschikt (beide)']
        colors3a = ['#d73027', '#d9ef8b', '#1a9850']
        plot_map(kd.sel(layer=l), colors3a, labels3a, overlays, f'{name}\Doorlaatvermogen {name} LHM-laag {int(l)}', logos=logos)
