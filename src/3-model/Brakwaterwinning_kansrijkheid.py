# -*- coding: utf-8 -*-
"""
Dit script combineert de scores van elke categorie voor brakwaterwinning tot
een kansrijkheidsscore door de volgende stappen te doorlopen:
    1. Laad alle score voor de categorien in
    2. Tel ze bij elkaar op

Input:
     1. Gemiddelde chlorideconcentratie brakwaterpakket_score (idf)
     2. Dikte brakwaterpakket_score (idf)
     3. Doorlaatvermogen_score (idf)
     4. Maximale diepte bovenkant brakwaterlaag_score (idf)

Output:
    - Kansrijkheidsscores Brakwaterwinning (idf)
"""
import xarray as xr
import numpy as np

# Input paths
path_brak_cat = r"data\3-input\brak_cat.nc"

# Output paths
path_brak = r"data\4-output\kansrijkheid_brakwaterwinning.nc"

# Laad files
brak_cat = xr.open_dataset(path_brak_cat)
brak_mean = brak_cat['brak_mean']
dikte_brak = brak_cat['dikte_brak']
kd = brak_cat['kd']
maxdiepte = brak_cat['max_diepte_brak']

# Bereken de scores aan de hand van de tabel (zie memo)
brakwater = xr.full_like(brak_mean, np.nan)
brakwater = brakwater.where(
    ~((brak_mean == 2) & (dikte_brak == 2) & (kd == 2) & (maxdiepte == 2)), 5
)
brakwater = brakwater.where(
    ~((brak_mean == 2) & (dikte_brak > 0) & (kd > 0) & (maxdiepte == 1)), 4
)
brakwater = brakwater.where(
    ~((brak_mean == 2) & (dikte_brak == 2) & (kd == 2) & (maxdiepte == 1)), 6
)
brakwater = brakwater.where(
    ~((brak_mean == 1) & (dikte_brak == 2) & (kd == 2) & (maxdiepte == 2)), 2
)
brakwater = brakwater.where(
    ~((brak_mean == 2) & (dikte_brak == 2) & (kd == 1) & (maxdiepte == 2)), 2
)
brakwater = brakwater.where(
    ~((brak_mean == 1) & (dikte_brak > 0) & (kd > 0) & (maxdiepte == 1)), 1
)
brakwater = brakwater.where(
    ~((brak_mean == 2) & (dikte_brak == 1) & (kd > 0) & (maxdiepte == 2)), 1
)
brakwater = brakwater.where(
    ~((brak_mean == 1) & (dikte_brak == 2) & (kd == 2) & (maxdiepte == 1)), 3
)
brakwater = brakwater.where(~(brak_mean == 0), 0)
brakwater = brakwater.where(~(dikte_brak == 0), 0)
brakwater = brakwater.where(~(maxdiepte == 0), 0)
brakwater = brakwater.where(
    ~(
        (brak_mean.notnull())
        & (dikte_brak.notnull())
        & (maxdiepte.notnull())
        & (kd == 0)
    ),
    0,
)

brakwater.to_netcdf(path_brak)
