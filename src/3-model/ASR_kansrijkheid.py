# -*- coding: utf-8 -*-
"""
Dit script combineert de scores van elke categorie voor ASR tot een kans-
rijkheidsscore door de volgende stappen te doorlopen:
    1. Laad alle score voor de categorien in
    2. Tel ze bij elkaar op

Input:
     1. Gemiddelde chlorideconcentratie brakwaterpakket_score (idf)
     2. Dikte brakwaterpakket_score (idf)
     3. Doorlaatvermogen_score (idf)
     4. Maximale diepte bovenkant brakwaterlaag_score (idf)

Output:
    - Kansrijkheidsscores ASR (idf)
"""
import xarray as xr
import numpy as np

# Input paths
path_ASR_cat = "data/3-input/ASR_cat.nc"

# Output paths
path_ASR = r"data\4-output\kansrijkheid_ASR.nc"


# Laad files
ASR_cat = xr.open_dataset(path_ASR_cat)
stroming = ASR_cat['stroming']
qmin = ASR_cat['qmin']
opbarsting = ASR_cat['opbarsting']
kd = ASR_cat['kd']

# Bereken de scores aan de hand van de tabel (zie memo)
asr = xr.full_like(qmin, np.nan).load()
asr = asr.where(~((qmin == 1) & (kd == 2)), 6)
asr = asr.where(~((qmin == 2) & (kd == 2)), 5)
asr = asr.where(~((qmin == 1) & (kd == 1)), 4)
asr = asr.where(~((qmin == 2) & (kd == 1)), 2)
asr = asr.where(
    ~(
        (stroming == 0)
        & (qmin == 1)
        & (kd == 2)
    ),
    3,
)
asr = asr.where(
    ~((stroming == 0) & (qmin == 2) & (kd == 2)), 2
    )
asr = asr.where(
    ~((stroming == 0) & (qmin == 1) & (kd == 1)), 1
    )
asr.loc[dict(layer=slice(1,2))] = asr.sel(layer=slice(1,2)).where(~(opbarsting == 0), 0)
asr = asr.where(~(qmin == 0), 0)
asr = asr.where(~((qmin.notnull()) & (kd == 0)), 0)
asr = asr.where(~((stroming == 1) & (qmin == 2) & (kd == 1)), 0)


asr.to_netcdf(path_ASR)

