# -*- coding: utf-8 -*-
"""
Dit script combineert de scores van elke categorie voor ASR tot een kans-
rijkheidsscore door de volgende stappen te doorlopen:
    1. Laad alle score voor de categorien in
    2. Tel ze bij elkaar op

Input:
     1. Gemiddelde chlorideconcentratie brakwaterpakket_score (idf)
     2. Dikte brakwaterpakket_score (idf)
     3. Doorlaatvermogen_score (idf)
     4. Maximale diepte bovenkant brakwaterlaag_score (idf)

Output:
    - Kansrijkheidsscores ASR (idf)
"""
import xarray as xr
import numpy as np
import imod
"""
# Input paths
path_stroming = r"data\3-input\ASR\achtergrondstroming.idf"
path_qmin = r"data\3-input\ASR\qmin\qmin_l*.idf"
path_opbarsting = r"data\3-input\ASR\opbarstings_index.idf"
path_kd = r"data\3-input\ASR\kd\kd_l*.idf"

# Output paths
path_ASR = r"data\4-output\ASR\kansrijkheid_ASR.idf"
path_ASR_totaal = r"data\4-output\ASR\kansrijkheid_ASR_totaal.idf"

# Laad files
stroming = imod.idf.open(path_stroming)
qmin = imod.idf.open(path_qmin)
opbarsting = imod.idf.open(path_opbarsting)
kd = imod.idf.open(path_kd)
"""

def calc_asr(stroming, qmin, opbarsting, kd):
    # Bereken de scores aan de hand van de tabel (zie memo)
    asr = xr.full_like(qmin, np.nan).load()
    asr = asr.where(~((qmin == 1) & (kd == 2)), 6)
    asr = asr.where(~((qmin == 2) & (kd == 2)), 5)
    asr = asr.where(~((qmin == 1) & (kd == 1)), 4)
    asr = asr.where(~((qmin == 2) & (kd == 1)), 2)
    asr = asr.where(
        ~(
            (stroming == 0)
            & (qmin == 1)
            & (kd == 2)
        ),
        3,
    )
    asr = asr.where(
        ~((stroming == 0) & (qmin == 2) & (kd == 2)), 2
        )
    asr = asr.where(
        ~((stroming == 0) & (qmin == 1) & (kd == 1)), 1
        )
    asr = asr.where(~(opbarsting == 0), 0)
    asr = asr.where(~(qmin == 0), 0)
    asr = asr.where(~((qmin.notnull()) & (kd == 0)), 0)
    asr = asr.where(~((stroming == 1) & (qmin == 2) & (kd == 1)), 0)

    asr_totaal = asr.sel(layer=1).drop('layer')
    return asr_totaal


def test_da():
    nrow, ncol = 3, 4
    dx, dy = 1.0, -1.0
    xmin, xmax = 0.0, 4.0
    ymin, ymax = 0.0, 3.0
    coords = imod.util._xycoords((xmin, xmax, ymin, ymax), (dx, dy))
    kwargs = {"name": "test", "coords": coords, "dims": ("y", "x")}
    data = np.ones((nrow, ncol), dtype=float)
    da = xr.DataArray(data, **kwargs)
    return da


def test_layerda():
    nlay, nrow, ncol = 5, 3, 4
    dx, dy = 1.0, -1.0
    xmin, xmax = 0.0, 4.0
    ymin, ymax = 0.0, 3.0
    coords = imod.util._xycoords((xmin, xmax, ymin, ymax), (dx, dy))
    coords["layer"] = np.arange(nlay) + 1
    kwargs = {"name": "layer", "coords": coords, "dims": ("layer", "y", "x")}
    data = np.ones((nlay, nrow, ncol), dtype=np.float32)
    da = xr.DataArray(data, **kwargs)
    return da


test_result = [
    [1,1,1,2,6],
    [1,2,1,2,5],
    [1,1,1,1,4],
    [0,1,1,2,3],
    [0,2,1,2,2],
    [0,1,1,1,1],
    #nul-categorieen
    [0,0,1,1,0],
    [1,0,1,1,0],
    [0,1,0,1,0],
    [1,1,0,1,0],
    [0,2,0,1,0],
    [1,2,0,1,0],
    [0,1,1,0,0],
    [1,1,1,0,0],
    [0,2,1,0,0],
    [1,2,1,0,0],
    [1,2,0,0,0],
]

for a,b,c,d,test in test_result:
    #break
    stroming = xr.full_like(test_da(), a)
    qmin = xr.full_like(test_layerda(), b)
    opbarsting = xr.full_like(test_da(), c)
    kd = xr.full_like(test_layerda(), d)
    testda = xr.full_like(test_da(), test)

    asr = calc_asr(stroming, qmin, opbarsting, kd)

    try:
        xr.testing.assert_equal(testda, asr)
    except AssertionError:
        print(f"Assertion error for a:{a}, b:{b}, c:{c}, d:{d}, should be {test}, is {asr.values[0,0]}")

