# -*- coding: utf-8 -*-
"""
Dit script combineert de scores van elke categorie voor brakwaterwinning tot
een kansrijkheidsscore door de volgende stappen te doorlopen:
    1. Laad alle score voor de categorien in
    2. Tel ze bij elkaar op

Input:
     1. Gemiddelde chlorideconcentratie brakwaterpakket_score (idf)
     2. Dikte brakwaterpakket_score (idf)
     3. Doorlaatvermogen_score (idf)
     4. Maximale diepte bovenkant brakwaterlaag_score (idf)

Output:
    - Kansrijkheidsscores Brakwaterwinning (idf)
"""
import xarray as xr
import numpy as np
import imod


def calc_brakwaterwinning(brak_mean, dikte_brak, kd, maxdiepte):
    # Bereken de scores aan de hand van de tabel (zie memo)
    brakwater = xr.full_like(brak_mean, np.nan)
    brakwater = brakwater.where(
        ~((brak_mean == 2) & (dikte_brak > 0) & (kd > 0) & (maxdiepte == 1)), 3
    )
    brakwater = brakwater.where(
        ~((brak_mean == 2) & (dikte_brak == 2) & (kd == 2) & (maxdiepte > 0)), 4
    )
    brakwater = brakwater.where(
        ~((brak_mean == 1) & (dikte_brak == 2) & (kd == 2) & (maxdiepte > 0)), 2
    )
    brakwater = brakwater.where(
        ~((brak_mean == 2) & (dikte_brak == 2) & (kd == 1) & (maxdiepte == 2)), 2
    )
    brakwater = brakwater.where(
        ~((brak_mean == 1) & (dikte_brak > 0) & (kd > 0) & (maxdiepte > 0)), 1
    )
    brakwater = brakwater.where(
        ~((brak_mean == 2) & (dikte_brak == 1) & (kd > 0) & (maxdiepte == 2)), 1
    )
    brakwater = brakwater.where(~(brak_mean == 0), 0)
    brakwater = brakwater.where(~(dikte_brak == 0), 0)
    brakwater = brakwater.where(~(maxdiepte == 0), 0)
    brakwater = brakwater.where(
        ~(
            (brak_mean.notnull())
            & (dikte_brak.notnull())
            & (maxdiepte.notnull())
            & (kd == 0)
        ),
        0,
    )

    kans_brak = brakwater.max(dim="layer")
    return kans_brak


def test_da():
    nrow, ncol = 3, 4
    dx, dy = 1.0, -1.0
    xmin, xmax = 0.0, 4.0
    ymin, ymax = 0.0, 3.0
    coords = imod.util._xycoords((xmin, xmax, ymin, ymax), (dx, dy))
    kwargs = {"name": "test", "coords": coords, "dims": ("y", "x")}
    data = np.ones((nrow, ncol), dtype=float)
    da = xr.DataArray(data, **kwargs)
    return da
def test_layerda():
    nlay, nrow, ncol = 5, 3, 4
    dx, dy = 1.0, -1.0
    xmin, xmax = 0.0, 4.0
    ymin, ymax = 0.0, 3.0
    coords = imod.util._xycoords((xmin, xmax, ymin, ymax), (dx, dy))
    coords["layer"] = np.arange(nlay) + 1
    kwargs = {"name": "layer", "coords": coords, "dims": ("layer", "y", "x")}
    data = np.ones((nlay, nrow, ncol), dtype=np.float32)
    da = xr.DataArray(data, **kwargs)
    return da

test_result = [
    [2,2,2,1,4],
    [2,2,2,2,4],
    [2,1,1,1,3],
    [2,2,1,1,3],
    [2,1,2,1,3],
    [2,2,2,1,3],
    [1,2,2,1,2],
    [1,2,2,2,2],
    [1,1,1,1,1],
    [1,1,2,1,1],
    [1,1,1,2,1],
    [1,1,2,2,1],
    #nul-categorieen
    [0,1,1,1,0],
    [0,2,1,1,0],
    [0,1,2,1,0],
    [0,2,2,1,0],
    [0,1,1,2,0],
    [0,2,1,2,0],
    [0,1,2,2,0],
    [0,2,2,2,0],
    [1,0,1,1,0],
    [2,0,1,1,0],
    [1,0,2,1,0],
    [2,0,2,1,0],
    [1,0,1,2,0],
    [2,0,1,2,0],
    [1,0,2,2,0],
    [2,0,2,2,0],
    [1,1,0,1,0],
    [2,1,0,1,0],
    [1,2,0,1,0],
    [2,2,0,1,0],
    [1,1,0,2,0],
    [2,1,0,2,0],
    [1,2,0,2,0],
    [2,2,0,2,0],
    [1,1,1,0,0],
    [2,1,1,0,0],
    [1,2,1,0,0],
    [2,2,1,0,0],
    [1,1,2,0,0],
    [2,1,2,0,0],
    [1,2,2,0,0],
    [2,2,2,0,0],
]

for a,b,c,d,test in test_result:
    #break
    brak_mean = xr.full_like(test_layerda(), a)
    dikte_brak = xr.full_like(test_layerda(), b)
    kd = xr.full_like(test_layerda(), c)
    maxdiepte = xr.full_like(test_layerda(), d)
    testda = xr.full_like(test_da(), test)

    brw = calc_brakwaterwinning(brak_mean, dikte_brak, kd, maxdiepte)

    try:
        xr.testing.assert_equal(testda, brw)
    except AssertionError:
        print(f"Assertion error for a:{a}, b:{b}, c:{c}, d:{d}, should be {test}, is {brw.values[0,0]}")


