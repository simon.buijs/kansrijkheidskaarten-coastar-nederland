# -*- coding: utf-8 -*-
"""
Dit script kent scores toe (0,1,2) aan elke categorie voor brakwaterwinning
door de volgende stappen te doorlopen. Voor elk van de 4 factoren worden in
dit script de scores toegekend:

Input:
     1. Gemiddelde chlorideconcentratie brakwaterpakket (nc)
     2. Dikte brakwaterpakket (nc)
     3. Doorlaatvermogen (nc)
     4. Maximale diepte bovenkant brakwaterlaag (nc)

Output:
     1. Gemiddelde chlorideconcentratie brakwaterpakket_score (nc)
     2. Dikte brakwaterpakket_score (nc)
     3. Doorlaatvermogen_score (nc)
     4. Maximale diepte bovenkant brakwaterlaag_score (nc)
"""
import xarray as xr
import numpy as np

# Input paths
path_cl_data = r"data\2-interim\chloride_data.nc"
path_kd = "data/2-interim/kd_waarden.nc"
path_dikte_brak = r"data\2-interim\dikte_brak.nc"
path_ondergrond = r"data\2-interim\ondergrond.nc"

# Output paths
path_brak_cat = r"data\3-input\brak_cat.nc"

# Grenzen categorien
grens_cl_min = 500  # mg/l
grens_cl_midden = 5000  # mg/l
grens_cl_max = 10000  # mg/l
grens_dikte_klein = 10  # m
grens_dikte_groot = 20  # m
grens_kd_klein = 100  # m2/d
grens_kd_groot = 500  # m2/d
grens_diepte_min = -50  # m
grens_diepte_max = -190  # m

# Laad files`
cl_data = xr.open_dataset(path_cl_data)
brak_mean = cl_data['brak_mean']
dikte_brak = xr.open_dataset(path_dikte_brak)['dikte_brak']
maxdiepte = cl_data['max_diepte_brak']

kd = xr.open_dataset(path_kd)['kd']
k = xr.open_dataset(path_ondergrond)['k']

# Maak lege dataset voor alle categorien
da = xr.Dataset()

# Gemiddelde chlorideconcentratie brakwaterpakket
brak_mean_cat = brak_mean.where(~(brak_mean < grens_cl_min), 0)                 
brak_mean_cat = brak_mean_cat.where(~(brak_mean > grens_cl_midden), 0)
brak_mean_cat = brak_mean_cat.where(
    ~((brak_mean > grens_cl_min) & (brak_mean < grens_cl_midden)), 2
)
brak_mean_cat = brak_mean_cat.where(
    ~((brak_mean > grens_cl_midden) & (brak_mean < grens_cl_max)), 1
)
da['brak_mean'] = brak_mean_cat

# Dikte brakwaterpakket
dikte_brak_cat = dikte_brak.where(~(dikte_brak < grens_dikte_klein), 0)
dikte_brak_cat = dikte_brak_cat.where(~(dikte_brak >= grens_dikte_klein), 1)
dikte_brak_cat = dikte_brak_cat.where(~(dikte_brak >= grens_dikte_groot), 2)
da['dikte_brak'] = dikte_brak_cat

# Doorlaatvermogen
kd_cat = kd.where(~(kd >= grens_kd_klein), 1)
kd_cat = kd_cat.where(~(kd > grens_kd_groot), 2)
kd_cat = kd_cat.where(~(kd < grens_kd_klein), 0)

da['kd'] = kd_cat

# Maximale diepte bovenkant brakwaterlaag
maxdiepte_cat = xr.full_like(maxdiepte, np.nan)
maxdiepte_cat = maxdiepte_cat.where(~(maxdiepte < grens_diepte_max), 0)
maxdiepte_cat = maxdiepte_cat.where(~(maxdiepte >= grens_diepte_max), 2)
maxdiepte_cat = maxdiepte_cat.where(~(maxdiepte > grens_diepte_min), 1)


da['max_diepte_brak'] = maxdiepte_cat

# Save data
da.to_netcdf(path_brak_cat)
