# -*- coding: utf-8 -*-
"""
Dit script kent scores toe (0,1,2) aan elke categorie voor ASR door de volgende
stappen te doorlopen. Voor elk van de 4 factoren worden in dit script de scores
 toegekend:

Input:
     1. Achtergrondstroming (idf)
     2. Rendementsverlies door opdrijving (idf)
     3. Opbarstrisico (idf)
     4. Doorlaatvermogen (idf)

Output:
     ASR_cat.nc
"""

import xarray as xr
import numpy as np

# Input paths
path_stroming = r"data\2-interim\achtergrondstroming.nc"
path_qmin = r"data\2-interim\qmin.nc"
path_opbarsting = r"data\index_ditch.nc"
path_kd = r"data\2-interim\kd_waarden.nc"
path_ondergrond = r"data\2-interim\ondergrond.nc"

# Output paths
path_ASR_cat = "data/3-input/ASR_cat.nc"

# Grenzen categorien
grens_stroming = 10  # m/j
grens_qmin_klein = 50000  # m3/j
grens_qmin_groot = 2500000  # m3/j
grens_opbarsting_wvp1 = 1.3  # index
# grens_opbarsting_wvp2 = 1.15  # index
# grens_opbarsting_wvp3 = 1.0

grens_kd_klein = 100  # m2/d
grens_kd_groot = 200  # m2/d

# Laad files
stroming = xr.open_dataset(path_stroming)['achtergrondstroming']
qmin = xr.open_dataset(path_qmin)['qmin']
opbarsting = xr.open_dataset(path_opbarsting)['index_ditch']
kd = xr.open_dataset(path_kd)['kd']
k = xr.open_dataset(path_ondergrond)['k']
wvp = xr.open_dataset(path_ondergrond)['wvp']
# Maak een dataset voor de data
da = xr.Dataset()

# Achtergrondstroming
stroming_cat = stroming.where(~(stroming < grens_stroming), 1)
stroming_cat = stroming_cat.where(~(stroming >= grens_stroming), 0)
da['stroming'] = stroming_cat

# Rendementsverlies door opdrijving
qmin_cat = xr.full_like(qmin, np.nan)
qmin_cat = qmin_cat.where(~(qmin <= grens_qmin_groot), 2)  # alleen grootschalig
qmin_cat = qmin_cat.where(~(qmin > grens_qmin_groot), 0)
qmin_cat = qmin_cat.where(~(qmin <= grens_qmin_klein), 1)  # klein en  grootschalig
da['qmin'] = qmin_cat

# Opbarstingsrisico
opbarstings_cat = xr.full_like(opbarsting, np.nan)
opbarstings_cat = opbarstings_cat.where(~(opbarsting >= grens_opbarsting_wvp1), 1)
opbarstings_cat = opbarstings_cat.where(~(opbarsting < grens_opbarsting_wvp1), 0)
da['opbarsting'] = opbarstings_cat


# Doorlaatvermogen
kd_cat = kd.where(~(kd >= grens_kd_klein), 1)
kd_cat = kd_cat.where(~(kd > grens_kd_groot), 2)
kd_cat = kd_cat.where(~(kd < grens_kd_klein), 0)
kd_cat = kd_cat.where(~(k < 5), 0)
kd_cat = kd_cat.where(~(kd==1), np.nan)
da['kd'] = kd_cat

da.to_netcdf(path_ASR_cat)
