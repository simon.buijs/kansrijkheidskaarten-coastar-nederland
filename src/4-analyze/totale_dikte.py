# -*- coding: utf-8 -*-
"""
Dit script berekend de dikte van de kansrijke gebieden en de minimale diepte
"""
import xarray as xr
import imod
import seaborn as sns
import matplotlib.pyplot as plt
import geopandas as gpd

def select_area(da, path):
    shape = gpd.read_file(path)
    da = da.sel(
        y=slice(shape.bounds.maxy[0], shape.bounds.miny[0]),
        x=(slice(shape.bounds.minx[0], shape.bounds.maxx[0])),
    )
    return da

#Input
path_brakwater =  r"data\4-output\kansrijkheid_brakwaterwinning.nc"
path_asr = r"data\4-output\kansrijkheid_ASR.nc"
path_ondergrond = 'data/2-interim/ondergrond.nc'
path_provincie = r"data\1-external\Kaarten\Provincies\Provincies.shp"

# Output
path_dikte =  r"data\4-output\kansrijk_brak_dikte.tif"
path_diepte = r"data\4-output\kansrijk_brak_diepte.tif"
path_dikte_asr =  r"data\4-output\kansrijk_asr_dikte.nc"
path_diepte_asr = r"data\4-output\kansrijk_asr_diepte.nc"



brakwater = xr.open_dataset(path_brakwater)['brak_mean']
asr = xr.open_dataset(path_asr)['qmin']
brakwater.max(dim='layer').plot.imshow()

def bereken_dikte(da, path_ondergrond, path_dikte, path_diepte):
    wvp = xr.open_dataset(path_ondergrond)
    top = wvp['top']
    bot = wvp['bot']
    kansrijk_min_diepte = top.where(da>0).max(dim='layer')
    kansrijk_max_diepte = bot.where(da>0).min(dim='layer')
    kansrijk_dikte = kansrijk_min_diepte - kansrijk_max_diepte
    kansrijk_min_diepte.to_netcdf(path_dikte)
    kansrijk_dikte.to_netcdf(path_diepte)
    return kansrijk_min_diepte, kansrijk_dikte

kansrijk_min_diepte, kansrijk_dikte = bereken_dikte(brakwater, path_ondergrond, path_dikte, path_diepte)
colors = ['#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6']
levels = [-200, -150, -100, -50]

sns.set(style="whitegrid", palette="pastel", color_codes=True)

# Mask
mask = gpd.read_file(path_provincie)
mask = mask[mask.PROVC_NM.isin(['Groningen','Friesland','Noord-Holland','Utrecht','Zuid-Holland','Zeeland','Flevoland'])]
mask = imod.prepare.rasterize(mask, like=kansrijk_min_diepte)
kansrijk_min_diepte = kansrijk_min_diepte.where(mask.notnull())

ax1, ax2 = imod.visualize.plot_map(kansrijk_min_diepte, colors, levels, overlays = overlays) 
ax2.figure.set_figwidth(10)
ax2.figure.set_figheight(10)
ax2.set_title('Minimale diepte geschiktheid Brakwaterwinning (m onder NAP)')
plt.savefig("data/5-visualization/Brakwaterwinning/mindiepte",dpi=300, bbox_inches='tight')


# Voor ASR
kansrijk_min_diepte, kansrijk_dikte = bereken_dikte(asr, path_ondergrond, path_dikte_asr, path_diepte_asr)

colors = ['#d7191c','#fdae61','#ffffbf','#abd9e9','#2c7bb6']
levels = [-200, -150, -100, -50]

sns.set(style="whitegrid", palette="pastel", color_codes=True)

# Mask
mask = gpd.read_file(path_provincie)
mask = mask[mask.PROVC_NM.isin(['Groningen','Friesland','Noord-Holland','Utrecht','Zuid-Holland','Zeeland'])]
mask = imod.prepare.rasterize(mask, like=kansrijk_min_diepte)
kansrijk_min_diepte = kansrijk_min_diepte.where(mask.notnull())

ax1, ax2 = imod.visualize.plot_map(kansrijk_min_diepte, colors, levels, overlays = overlays) 
ax2.figure.set_figwidth(10)
ax2.figure.set_figheight(10)
ax2.set_title('Minimale diepte geschiktheid ASR (m onder NAP)')
plt.savefig("data/5-visualization/ASR/mindiepte",dpi=300, bbox_inches='tight')