"""
Script to separate layermodel in - overlapping - aquifers,
"""
import xarray as xr
import numba
import numpy as np



def sum_wvp(variable, ondergrond):
    """
    Function to seperate layermodel in aquifers. Sums all the values in one 
    WVP together. 
    Input  variable: 'string' with name of variable
            ondergrond: schematisation of subsoil-dataarray including wvp
    Output
            da with variable
    """
    da = ondergrond[variable]
    for i in range(1,int(ondergrond['wvp'].max().values+1)):
        da = da.where(~(ondergrond.wvp == i),ondergrond[variable].where(ondergrond.wvp==i).sum(dim='layer'))
    da = da.where(~ondergrond[variable].isnull(), np.nan)
    return da


def max_wvp(variable, ondergrond):
    """
    Function to seperate layermodel in aquifers. Sums all the values in one 
    WVP together. 
    Input  variable: 'string' with name of variable
            ondergrond: schematisation of subsoil-dataarray including wvp
    Output
            da with variable
    """
    da = ondergrond[variable]
    for i in range(1,int(ondergrond['wvp'].max().values+1)):
        da = da.where(~(ondergrond.wvp == i),ondergrond[variable].where(ondergrond.wvp==i).max(dim='layer'))
    da = da.where(~ondergrond[variable].isnull(), np.nan)
    return da

path_ondergrond = r"data\2-interim\ondergrond.nc"
path_cl_data = r"data\2-interim\chloride_data.nc"
path_kd = "data/2-interim/kd_waarden.nc"
path_d = "data/2-interim/dikte.nc"
path_dikte_brak = r"data\2-interim\dikte_brak.nc"
path_weerstand = r"data\2-interim\weerstand.nc"

ondergrond = xr.open_dataset(path_ondergrond)
ondergrond['dikte_brak'] = xr.open_dataset(path_cl_data)['dikte_brak']

dikte_brak = sum_wvp('dikte_brak', ondergrond)
dikte_brak.to_netcdf(path_dikte_brak)

kd_new = sum_wvp('kd',ondergrond)
kd_new.to_netcdf(path_kd)

d_new = sum_wvp('d',ondergrond)
d_new.to_netcdf(path_d)

vcw_new = max_wvp('vcw', ondergrond)
vcw_new.to_netcdf(path_weerstand)
