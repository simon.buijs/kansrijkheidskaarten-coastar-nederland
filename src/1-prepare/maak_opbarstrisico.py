# -*- coding: utf-8 -*-
"""
Created on Mon Jun 15 15:17:30 2020
Bereken opbarstrisico NL
@author: buijs_sn
"""
import imod
import numpy as np
import xarray as xr
import pandas as pd
import geopandas as gpd
import warnings
import glob
import matplotlib.pyplot as plt

warnings.filterwarnings("ignore",category =RuntimeWarning)

def select_area(da, shape):
    
    da = da.sel(
        y=slice(shape.bounds.maxy.item(), shape.bounds.miny.item()),
        x=(slice(shape.bounds.minx.item(), shape.bounds.maxx.item())),
    )
    return da

def set_rho_wet_dry(subsoil, df_specific_mass):
    # Placeholder for geology, not used for specific mass
    df_specific_mass["geology"] = 1
    rho_wet_table = imod.prepare.subsoil.build_lookup_table(
        df_specific_mass, "code", "geology", "rho_wet (kg/m3)"
    )
    rho_dry_table = imod.prepare.subsoil.build_lookup_table(
        df_specific_mass, "code", "geology", "rho_dry (kg/m3)"
    )
    placeholder = xr.full_like(subsoil["lithology"], 1).where(
        subsoil["lithology"] != -1, other=-1
    )
    subsoil["rho_wet"] = imod.prepare.subsoil.fill_in_k(
        placeholder, subsoil["lithology"], rho_wet_table
    )
    subsoil["rho_dry"] = imod.prepare.subsoil.fill_in_k(
        placeholder, subsoil["lithology"], rho_dry_table
    )
    return subsoil


def compute_holocene_weight(subsoil):
    """
    Parameters
    ----------
    subsoil : xr.Dataset
        Containing DataArrays of:
        * "thickness" 
            thickness of voxels [m]
        * "phreatic_level" 
            phreatic level [m NAP]
        * "ztop"
            top elevation of voxels [m NAP]
        * "zbot"
            bottom elevation of voxels [m NAP]
        * "rho_wet"
            specific wet mass of voxel [kg/m3]
        * "rho_dry"
            specific dry mass of voxel [kg/m3]
        * "hol_base"
            Holocene base [m NAP]
    Returns
    -------
    holocene_weight : xr.DataArray
        Weight of holocene cover layer [kg/m2]
    """
    # Calculate failure index for land area
    subsoil = subsoil.copy()  # Avoid side-effects
    subsoil["saturated_thickness"] = subsoil["thickness"].where(
        subsoil["ztop"] <= subsoil["phreatic_level"]
    )
    subsoil["partially_saturated"] = (
        subsoil["phreatic_level"] - subsoil["zbot"]
    ).where(
        (subsoil["ztop"] > subsoil["phreatic_level"])
        & (subsoil["zbot"] < subsoil["phreatic_level"])
    )
    subsoil["saturated_thickness"] = subsoil["saturated_thickness"].combine_first(
        subsoil["partially_saturated"]
    )
    subsoil["unsaturated_thickness"] = subsoil["thickness"].where(
        subsoil["zbot"] >= subsoil["phreatic_level"]
    )
    subsoil["unsaturated_thickness"] = subsoil["unsaturated_thickness"].combine_first(
        subsoil["thickness"] - subsoil["partially_saturated"]
    )

    subsoil["wet_weight"] = subsoil["rho_wet"] * subsoil["saturated_thickness"] 
    subsoil["dry_weight"] = subsoil["rho_dry"] * subsoil["unsaturated_thickness"] 
    subsoil["weight"] = (subsoil["wet_weight"] + subsoil["dry_weight"]).where(
        (subsoil["ztop"] > subsoil["phreatic_level"])
        & (subsoil["zbot"] < subsoil["phreatic_level"])
    )
    subsoil["weight"] = subsoil["weight"].combine_first(subsoil["wet_weight"])
    subsoil["weight"] = subsoil["weight"].combine_first(subsoil["dry_weight"])
    subsoil["holocene_weight"] = (
        subsoil["weight"].where(subsoil["ztop"] > subsoil["base_holocene"])
    ).sum("z")
    return subsoil


def compute_holocene_weight_ditch(subsoil, stage, ditch_depth):
    """
    Compute holocene weight under a ditch.
    Less weight present due to water instead of soil.
    Assumes fully saturated conditions (under a ditch).

    Parameters
    ----------
    subsoil: xr.Dataset (see above)
    stage: xr.DataArray
        Stages of the surface waters [m NAP]
    ditch depth:
        Assumed depth of surface waters [m]

    Returns
    -------
    holocene_weight : xr.DataArray
        Holocene weight assuming ditch depth
    """
    subsoil = subsoil.copy()
    subsoil["ditch_bed_level"] = stage - ditch_depth
    subsoil["ditch_saturated_thickness"] = subsoil["thickness"].where(
        subsoil["ztop"] <= subsoil["ditch_bed_level"]
    )
    # calculate weight of voxel which is partly cut into by ditch
    subsoil["ditch_voxel_soil_thickness"] = (
        subsoil["ditch_bed_level"] - subsoil["zbot"]
    ).where(
        (subsoil["ztop"] > subsoil["ditch_bed_level"])
        & (subsoil["zbot"] < subsoil["ditch_bed_level"])
    )
    subsoil["ditch_saturated_thickness"] = subsoil[
        "ditch_saturated_thickness"
    ].combine_first(subsoil["ditch_voxel_soil_thickness"])
    subsoil["weight_under_ditch"] = (
        subsoil["rho_wet"] * subsoil["ditch_saturated_thickness"]
    )
    holocene_weight_ditch = (
        subsoil["weight_under_ditch"].where(subsoil["ztop"] > subsoil["base_holocene"])
    ).sum("z") + ditch_depth * 1000.0
    return holocene_weight_ditch

# Input paths
path_geotop = "data/1-external/GeoTOP.nc"
path_specific_mass = "data/1-external/specific_mass_lithology.csv"
path_base_holocene = r"data\1-external\Opbarstrisico\HLc\HLc-bot.idf"
path_head = "data/1-external/Opbarstrisico/HEAD_STEADY-STATE_l*.idf"
path_zarr = r'data\1-external\GeoTOP_flevoland.nc' 
path_ditch_tertiary =  r'data\1-external\Peil\BODH_T1Z_250.IDF'
path_ditch_secundary = r'data\1-external\Peil\BODH_S1Z_250.IDF'
path_stage_tertiary =  r'data\1-external\Peil\PEIL_T1S_250.IDF'
path_stage_secundary = r'data\1-external\Peil\PEIL_S1Z_250.IDF'
path_provincie = r"data\1-external\Kaarten\Provincies\Provincies.shp"

# Load data
head = imod.idf.open(path_head).squeeze('time').drop('time')
_, xmin, xmax, _, ymin, ymax = imod.util.spatial_reference(head)

df_specific_mass = pd.read_csv(path_specific_mass)
df_specific_mass = df_specific_mass[df_specific_mass.columns[1:]].astype(int)

holocene_base = imod.idf.open(path_base_holocene).sel(
    y=slice(ymax, ymin), x=slice(xmin, xmax)
)
holocene_base = imod.prepare.Regridder(method="mode").regrid(holocene_base, head.sel(layer=2))
geotop_data = xr.open_dataset(path_geotop, chunks={'x':2800,'y':3250})['lithology']
geotop_zarr = xr.open_dataset(path_zarr, chunks={'x':2800,'y':3250})['lithology']


mask = gpd.read_file(path_provincie)
mask = mask[mask.PROVC_NM.isin(['Groningen','Friesland','Noord-Holland','Utrecht','Zuid-Holland','Zeeland','Flevoland','Noord-Brabant'])]
mask = imod.prepare.rasterize(mask, like=head.sel(layer=2))

stage_tertiary = imod.idf.open(path_stage_tertiary)
stage_secundary = imod.idf.open(path_stage_secundary)
stage = xr.ufuncs.fmin(stage_tertiary, stage_secundary)
stage = imod.prepare.fill(stage)
stage = stage.where(mask.notnull())
ditch_tertiary = imod.idf.open(path_ditch_tertiary)
ditch_secundary = imod.idf.open(path_ditch_secundary)
ditch = xr.ufuncs.fmin(ditch_tertiary, ditch_secundary)
ditch = imod.prepare.fill(ditch)
ditch = ditch.where(mask.notnull())
ditch_depth = stage-ditch
del ditch, stage_tertiary, stage_secundary, mask

def calculate_hydraulic_failure(minx, maxx, i):
    h = head.sel(x=slice(minx,maxx))
    hb = holocene_base.sel(x=slice(minx,maxx))
    
    # Regrid geotop to 250x250 to minimize memory usage
    
    geotop = geotop_data.sel(x=slice(minx,maxx), z=slice(-40,30))
    zarr = geotop_zarr.sel(x=slice(minx,maxx), z=slice(-40,30))
    geotop = geotop.combine_first(zarr).astype(np.int8)
    
    regridder = imod.prepare.Regridder(method="mode")
    lithology = regridder.regrid(geotop, h.sel(layer=2))

    geotop = xr.Dataset()
    geotop['lithology'] = lithology.astype(np.int8)
    
    
    # Add data
    geotop["ztop"] = geotop["z"] + 0.25
    geotop["zbot"] = geotop["z"] - 0.25
    geotop["phreatic_level"] = h.sel(layer=1)
    geotop["base_holocene"] = hb
    
 
    # Set specific mass
    geotop = set_rho_wet_dry(geotop.load(), df_specific_mass)
    geotop['rho_wet'] = geotop['rho_wet'].astype(np.int16)
    geotop['rho_dry'] = geotop['rho_dry'].astype(np.int16)
    
    # Include additional info into datasets
    geotop["thickness"] = xr.full_like(geotop["lithology"], 0.5, dtype=np.float).where(
        geotop["lithology"] != -1
    )
    
    geotop = compute_holocene_weight(geotop)
    holocene_weight_ditch = compute_holocene_weight_ditch(geotop, stage.sel(x=slice(minx,maxx)), ditch_depth.sel(x=slice(minx,maxx)))
    
    # Determine top of aquifer
    
    # Select the heads at that location
    aquifer_top_head = h.sel(layer=2)
    
    
    
    # Compute failure index for regular land area
    rho_fresh = 1000.0  # Fresh water specific mass [kg / m3]
    
    elevation_head = holocene_base
    pressure_head = aquifer_top_head - elevation_head
    index = geotop["holocene_weight"] / (pressure_head * rho_fresh)
    ditch_index = holocene_weight_ditch / (pressure_head * rho_fresh)
    # Store it
    ds = xr.Dataset()
    ds['index_ditch'] = ditch_index
    ds["pressure_head"] = pressure_head
    ds["holocene_weight"] = geotop["holocene_weight"]
    ds["holocene_weight_ditch"] = geotop["holocene_weight_ditch"]
    ds["elevation_head"] = elevation_head
    ds["index"] = index
    print('save to file')
    ds.to_netcdf(fr"data\2-interim\opbarsting_{i+1}.nc")
    
     
for i, (minx, maxx) in enumerate(zip([xmin,xmax/4,xmax/2, xmax*3/4],[xmax/4-1,xmax/2-1, xmax*3/4-1,xmax])):
    print(minx,maxx)
    calculate_hydraulic_failure(minx,maxx,i)
    
   

paths = glob.glob(r"data\2-interim\opbarsting_*.nc")

opbarsting = xr.open_mfdataset(paths)
opbarsting.to_netcdf(r"data\2-interim\opbarsting.nc")

# grens_opbarsting = 1.3
# opbarstings_cat = opbarsting['index_ditch_0.5m'].where(~(opbarsting['index_ditch_0.5m'] >= grens_opbarsting), 1)
# opbarstings_cat = opbarstings_cat.where(~(opbarsting['index_ditch_0.5m'] < grens_opbarsting), 0)
# opbarstings_cat.plot.imshow()

# colors = ['#d73027','#fc8d59','#fee090','#e0f3f8','#91bfdb','#4575b4']

# def plot_opbarsting(da, levels=None):
#     da.load()
#     if levels == None:
#         levels = da.quantile([0, 0.2,0.4,0.6,0.8]).values
#     ax1, ax2 = imod.visualize.plot_map(da, colors,levels)
#     ax2.set_title(da.name)
#     ax2.figure.set_figwidth(10)
#     ax2.figure.set_figheight(10)
#     plt.savefig(rf"data\5-visualization\{da.name}",  bbox_inches='tight')

# plot_opbarsting(opbarsting['pressure_head'], levels=[])
# plot_opbarsting(opbarsting['holocene_weight'], levels=[0,1000,20000,50000,100000])
# plot_opbarsting(opbarsting['index'].where(opbarsting['index']>0.9), levels=[0.9,1.1,1.4,1.5,1.6])
# plot_opbarsting(opbarsting['elevation_head'])

opbarsting['pressure_head'].to_netcdf(rf"data\pressure_head.nc")
opbarsting['elevation_head'].to_netcdf(rf"data\elevation_head.nc")
opbarsting['index'].to_netcdf(rf"data\index.nc")
opbarsting['holocene_weight'].to_netcdf(rf"data\holocene_weight.nc")
head.sel(layer=2).to_netcdf(rf"data\stijghoogte.nc")
opbarsting['index_ditch'].to_netcdf(r"data\index_ditch.nc")
