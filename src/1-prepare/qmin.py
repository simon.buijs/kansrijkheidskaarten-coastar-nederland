# -*- coding: utf-8 -*-
"""
Dit script berekent de minimale Q om het rendementsverlies door opdrijving
beperkt te houden (methode Bakker).

Input:
    Gemiddelde chloride-concentratie per WVP (asc)
    Gemiddelde K-waarde per WVP (asc)
    Dikte van het watervoerende pakket (asc)

Output:
   Qmin (nc)

"""
import xarray as xr

# Input paths
path_cl_data = r"data\2-interim\chloride_data.nc"
path_ondergrond = r"data\2-interim\ondergrond.nc"
path_d = "data/2-interim/dikte.nc"

# Output paths
path_qmin = r"data\2-interim\qmin.nc"

# Laad data
cl_data =  xr.open_dataset(path_cl_data)
cl_mean = xr.open_dataset(path_cl_data)['cl_mean']
ondergrond = xr.open_dataset(path_ondergrond)
k = ondergrond["k"]
dikte = xr.open_dataset(path_d)['d']

# methode bakker
# D >= 14.3
# D = Q/ Kx a H2
# Qmin = D*Q /Kx * H2
density = 1000 + 0.00134 * cl_mean
alpha = (density - 1000) / 1000
qmin = 14.3 * k * alpha * (dikte * dikte) * 365
qmin.name = "qmin"
qmin.to_netcdf(path_qmin)
