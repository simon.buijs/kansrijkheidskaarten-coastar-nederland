# -*- coding: utf-8 -*-
"""
Dit script bereidt de achtergrondstroming voor in de juiste vorm.

Input:
   Achtergrondstroming (asc)

Output:
   Opbarstrisico met juiste resolutie (idf)

"""
import xarray as xr
import numpy as np
import geopandas as gpd
import imod


# Input paths
path_stroming_in = r"data\1-external\Stroomsnelheid\Stromingsnelheid_lhm4_0_l*.idf"
path_cl_data = r"data\2-interim\chloride_data.nc"

# Output paths
path_stroming = r"data\2-interim\achtergrondstroming.nc"

# Laad data
stroming = imod.idf.open(path_stroming_in)

stroming = stroming.where(stroming > 0, np.nan)
stroming = stroming.where(stroming < 1000, np.nan)

# Data opslaan
stroming = stroming.squeeze()
stroming.name = "achtergrondstroming"
stroming.to_netcdf(path_stroming)
