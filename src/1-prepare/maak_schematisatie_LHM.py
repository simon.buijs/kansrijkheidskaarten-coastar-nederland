# -*- coding: utf-8 -*-
"""
Dit script maakt de ondergrondschematisatie op basis van LHM 4.0 door
middel van de volgende stappen:
1. Laad de data van LHM
2. Regrid het naar de goede resolutie (van de chloride)

Input:
    Shapefile van modelgebied
    LHM top, bots

Output:
    Ondergrondschematisatie (netcdf) met daarin per WVP:
        top (m NAP)
        bot (m NAP)
        doorlatend vermogen (m2/dag)
        dikte (m)
    En deze apart als idf's opgeslagen
"""
import xarray as xr
import geopandas as gpd
import imod
import numpy as np


# Input paths
path_lhm = r"data\1-external\LHM_schematisatie\**\*.idf"

# TODO: change path like
# Output paths
path_output = r"data\2-interim\lhm.nc"

# Laad data
das = imod.idf.open_dataset(path_lhm)

bot = das['mdl_bot']
top =das['mdl_top']
kd = das['mdl_kd']
vcw = das['vcw_mdl']

da = xr.Dataset()
da["top"] = top
da["bot"] = bot
da["kd"] = kd
da["d"] = da["top"] - da["bot"]
da['vcw'] = vcw
da["k"] = da["kd"] / da["d"]

da.to_netcdf(path_output)
