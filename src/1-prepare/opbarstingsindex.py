# -*- coding: utf-8 -*-
"""
Dit script bereidt het opbarstingsrisico voor in de juiste vorm.

Input:
   Opbarstingsrisico (asc)

Output:
   Opbarstrisico met juiste resolutie (nc)

"""
import xarray as xr
import imod
import numpy as np

# Input paths
path_opbarst_in = r"data\1-external\opbarstings_index_peilvast_T0_ditch1_0m.asc"
path_cl_data = r"data\2-interim\chloride_data.nc"

# Output paths
path_opbarsting = r"data\2-interim\opbarstings_index.nc"

# Laad data
like = xr.open_dataset(path_cl_data)['cl_mean']
opbarsting = imod.rasterio.open(path_opbarst_in)

# Regridden en nodata eruit halen
median_regridder = imod.prepare.Regridder(method="median")
opbarsting = opbarsting.where(~(opbarsting == 99999), np.nan)
opbarsting = opbarsting.where(~(opbarsting == 9999), np.nan)
opbarsting = median_regridder.regrid(opbarsting, like.sel(layer=1))
opbarsting.name = "opbarsting"
# Data opslaan
opbarsting.to_netcdf(path_opbarsting)
