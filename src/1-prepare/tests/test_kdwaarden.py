# -*- coding: utf-8 -*-

import xarray as xr
import numpy as np
import imod
import numba


def define_aquifer_kd(aq_sep, kd_in):
    # for each layer:
    # look both above and below. Include layers above/below if not aq_separation
    # save kd for each layer

    @numba.njit
    def _set_kd(start, aq_sep, kd, kd_out):
        nlay, nrow, ncol = aq_sep.shape
        for i in range(nrow):
            for j in range(ncol):
                # first down
                for k in range(start-1, nlay-1):
                    n = aq_sep[k, i, j]
                    if np.isnan(kd[k+1, i, j]):
                        break
                    if n == True:
                        break
                    else:
                        kd_out[i, j] += kd[k+1, i, j]

                # then up
                for k in range(start-2, -1, -1):
                    if not k:
                        n == True
                    else:
                        n = aq_sep[k, i, j]
                    if np.isnan(kd[k-1, i, j]):
                        break
                    if n == True:
                        break
                    else:
                        kd_out[i, j] += kd[k-1, i, j]

    def set_kd(start, aq_sep, kd):
        kd_out = kd.sel(layer=start)
        _set_kd(start, aq_sep.values, kd.copy().values, kd_out.values)
        return kd_out

    kds = []
    for l in range(1, kd_in.layer.size+1):
        kd_a = set_kd(l, aq_sep, kd_in.copy())
        kds.append(kd_a)
    kd_new = xr.concat(kds, dim="layer")
    return kd_new


def test_da():
    nlay, nrow, ncol = 5, 3, 4
    dx, dy = 1.0, -1.0
    xmin, xmax = 0.0, 4.0
    ymin, ymax = 0.0, 3.0
    coords = imod.util._xycoords((xmin, xmax, ymin, ymax), (dx, dy))
    coords["layer"] = np.arange(nlay) + 1
    kwargs = {"name": "layer", "coords": coords, "dims": ("layer", "y", "x")}
    data = np.ones((nlay, nrow, ncol), dtype=np.float32)
    da = xr.DataArray(data, **kwargs)
    return da


test_result = [
    [True, 1., 1.],
    [False, 1. ,5.],
    [True, 2., 2.],
    [False, 2., 10.]
]

for a,b,c in test_result:
    aq_sep = xr.full_like(test_da(), a)
    kd_in = xr.full_like(test_da(),b)
    testda = xr.full_like(test_da(),c)
    kd_out = define_aquifer_kd(aq_sep, kd_in)

    try:
        xr.testing.assert_equal(testda, kd_out)
    except AssertionError:
        print(f"Assertion error for aq_sep:{a}, kd_in:{b}. Testresult is {(kd_out.sum()/kd_out.size).values}, should be {c}")
