# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 15:48:53 2020
Script om de verhouding tussen de chloride-concentratie in het WVP
en de onderliggende laag te berekenen

Input:
    a. Gemiddelde chloride-concentratie brakwaterpakket per WVP (idf)
    b. Gemiddelde chloride-concentratie per WVP (idf)

Output:
    Ratio cl-conc brakwaterpakket en cl-conc onderliggende WVP  (idf)
"""

import xarray as xr
import imod
import numpy as np

# Input paths
path_cl_data = r"data\2-interim\chloride_data.nc"
# Output paths
path_ratio_cl = r"data\2-interim\ratio_cl.nc"

chloride = xr.open_dataset(path_cl_data)
cl_mean = chloride['cl_mean']
brak_mean = chloride['brak_mean']

# Bereken verhouding brakwaterpakket met onderliggende pakket
ratio_cl = xr.full_like(cl_mean.sel(layer=slice(1, 7)), np.nan).load()

for i in range(1, 7):
    ratio_cl.loc[{"layer": i}] = cl_mean.sel(layer=i + 1).drop("layer") / brak_mean.sel(
        layer=i
    ).drop("layer")

ratio_cl.to_netcdf(path_ratio_cl)
