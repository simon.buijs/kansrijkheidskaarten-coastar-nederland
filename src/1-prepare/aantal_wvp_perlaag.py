# -*- coding: utf-8 -*-
"""
Bepalen watervoerend-pakket lagen

Invoer: Ondergrond (nc)
        inclusief vcw

Aanname: Cutoff-waarde weerstand

Uitvoer:Ondergrond (nc) incl WVP per laag
"""
import xarray as xr
 
# Invoer
path_lhm= 'data/2-interim/lhm.nc'
path_wvp = 'data/2-interim/wvp.nc'


path_ondergrond = 'data/2-interim/ondergrond.nc'
c_cutoff = 200.  # days

ondergrond = xr.open_dataset(path_lhm)
vcw = ondergrond['vcw']

aq_sep = vcw > c_cutoff


wvp = xr.zeros_like(ondergrond['kd'])
wvp.loc[dict(layer=1)] = wvp.sel(layer=1).where(~(ondergrond['d'].sel(layer=1) >5), 1)
for i in range(2,9):
    print(i)
    wvp.loc[dict(layer=i)] = wvp.sel(layer=i-1).where(~aq_sep.sel(layer=i-1),wvp.sel(layer=i-1)+1)

ondergrond['wvp']= wvp

wvp.to_netcdf(path_wvp) 
ondergrond.to_netcdf(path_ondergrond)
