# -*- coding: utf-8 -*-
"""
Dit script bepaald de weerstand van de holocene deklaag, door het volgen van
de volgende stappen:
1. Bepaal onderkant holoceen d.m.v. REGIS
2. Bepaal weerstand van geotop lagen
3. Bereken weerstand holocene deklaag

Input:
    Shapefile van modelgebied
    REGIS (netcdf)
    GeoTOP (netcdf)
    Tabel met GeoTOP-k-waarden (csv)

Output:
    Weerstand Holocene deklaag in dagen (asc)
"""

import xarray as xr
import geopandas as gpd
import imod
import pandas as pd


def select_area(da):
    fn_pzh = r"data\1-external\Model_grens\Model_grens.shp"
    pzh = gpd.read_file(fn_pzh)
    da = da.sel(
        y=slice(pzh.bounds.maxy[0], pzh.bounds.miny[0]),
        x=(slice(pzh.bounds.minx[0], pzh.bounds.maxx[0])),
    )
    return da


# Input paths
path_regis = r"data\1-external\REGIS_v2_2.nc"
path_geotop = r"data\1-external\GeoTOP.nc"
path_geotop_table = r"data\1-external\GeoTOP_k_values.csv"

# Output paths
path_c_deklaag = r"data\2-interim\holocene_deklaag_c.nc"

# Selecteer diepte van overgang holoceen o.b.v. REGIS
holoceen = select_area(xr.open_dataset(path_regis)).sel(formation="HLc")
zmin = holoceen["bot"].min()
zmax = holoceen["top"].max()
# Laad geotop in en bereken de weerstand
geotop = select_area(xr.open_dataset(path_geotop)).sel(z=slice(zmax, zmin, -1))
df = pd.read_csv(path_geotop_table)
kh_table = imod.prepare.subsoil.build_lookup_table(
    df, "lithoclass", "unit_nl", "k_horizontal"
)
kv_table = imod.prepare.subsoil.build_lookup_table(
    df, "lithoclass", "unit_nl", "k_vertical"
)
geotop["kh"] = imod.prepare.subsoil.fill_in_k(
    geotop["lithostrat"], geotop["lithology"], kh_table
)
geotop["kv"] = imod.prepare.subsoil.fill_in_k(
    geotop["lithostrat"], geotop["lithology"], kv_table
)
geotop["c"] = 0.5 / geotop["kv"]
geotop["kd"] = 0.5 * geotop["kh"]


depth = xr.full_like(geotop["c"], 0)
for z in depth.z:
    depth.loc[{"z": z}] = z
sel = depth > zmin

geotop_c = geotop["c"].where(sel).sum(dim="z")
geotop_c.to_netcdf(path_c_deklaag)
