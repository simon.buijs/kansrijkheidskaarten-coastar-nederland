# -*- coding: utf-8 -*-
"""
Dit script zorgt ervoor dat de chloride invoer in de juiste vorm komt te staan
door middel van de volgende stappen:
    1. Laden data
    2. Verrasteren chloride-data naar ondergrondschematisatie
    3. Bepalen chloride-eigenschappen per WVP

    Input:
        Chloride-data (netcdf)
        Ondergrondschematisatie (netcdf)

    Output:
        Chloride-data verrasterd
        Chloride-data per WVP (netcdf):
            - Gemiddelde cl-concentratie per WVP 
            - Gemiddelde cl-concentratie in brakwaterpakket 
            - Dikte brakwaterpakket
            - Diepte bovenkant brakwaterpakket 
            - Grensvlak brakwater
"""
import xarray as xr
import imod
import numpy as np
# Input paths
path_cl = r"data\1-external\Chlorideverdeling NL\3dchloride_for_crosssections.nc"
path_wvp = r"data\2-interim\lhm.nc"

# Output paths
path_cl_data = r"data\2-interim\chloride_data.nc"

# Laad data
chloride = xr.open_dataset(path_cl)['3d-chloride'].sel(percentile='p50')
chloride = chloride.swap_dims({'layer':'z'})

wvp = xr.open_dataset(path_wvp)
cl_data = xr.Dataset()


depth = xr.full_like(chloride, 0)
for z in depth.z:
    depth.loc[dict(z=z)] = z.values



depth = depth.swap_dims({'z':'layer'})
chloride = chloride.swap_dims({'z':'layer'})
sel_brak = (chloride > 500) & (chloride < 10000)
brak = chloride.where(sel_brak)

# Diepte over de lagen heen om straks de juiste WVP te selecteren
cltop = xr.full_like(chloride, 0)
for layer, top in zip(cltop.layer, cltop.top):
    cltop.loc[dict(layer=layer)] = top.values

clbot = xr.full_like(chloride, 0)
for layer, bot in zip(clbot.layer, clbot.bottom):
    clbot.loc[dict(layer=layer)] = bot.values

# Bereken gemiddelde chloride-concentratie, regrid naar LHM-laag
mean_regridder = imod.prepare.LayerRegridder(method="mean")
cl_data['cl_mean'] = mean_regridder.regrid(chloride, cltop,clbot,wvp["top"],wvp["bot"])


# Bepaal gemiddelde cl-concentratie van het brakwaterpakket per WVP
cl_data['brak_mean'] = mean_regridder.regrid(brak, cltop,clbot,wvp["top"],wvp["bot"])

# Bereken dikte brakwaterpakket en diepte bovenkant brakwaterpakket


# Selectie: per watervoerend pakket de juiste dieptes selecteren
sel_top = depth <= wvp["top"]
sel_bot = depth >= wvp["bot"]

diepte_per_WVP = depth.where(sel_top & sel_bot)
diepte_brakwater = diepte_per_WVP.where(sel_brak)
maxz = diepte_brakwater.max(dim="z")
minz = diepte_brakwater.min(dim="z")
cl_data['dikte_brak'] = maxz - minz
cl_data['max_diepte_brak'] = maxz

# Schrijf data weg
cl_data.to_netcdf(path_cl_data)


